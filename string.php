<?php
echo 'father said: "do your work"<br>'; // this is single quoted string
echo " that's why i did<br>"; // this is double quoted string
$str = <<<EOD
i enjoy my work <br>
it's interesting<br>
EOD;
echo $str; /* this is heredoc string */

$str =<<<'EOD'
i feel proud on my work
EOD;
echo "$str"; # this is newdoc string

?>

